#!/bin/bash
set -euo pipefail

export LANG=C.UTF-8
export LC_ALL=C.UTF-8
export LANGUAGE=C.UTF-8
cd "$(dirname "$0")"

# 防呆
if [ ! -f .env ]; then
  echo "必須先設定 docker 的 env 檔案"
  echo ""
  echo "    cp .env.example .env"
  echo "    vim .env"
  exit 1
fi

# 初始化 submodule
git submodule init
git submodule update

# 複製設定檔
if [ ! -f admin-dashboard/.env ]; then
  cp admin-dashboard/.env.example admin-dashboard/.env
  echo "已自動將 admin-dashboard/.env.example 複製到 admin-dashboard/.env，請記得修改。"
fi
if [ ! -f message-gateway/.env ]; then
  cp message-gateway/.env.example message-gateway/.env
  echo "已自動將 message-gateway/.env.example 複製到 message-gateway/.env，請記得修改。"
fi
if [ ! -f chatbot/.env ]; then
  cp chatbot/.env.example chatbot/.env
  echo "已自動將 chatbot/.env.example 複製到 chatbot/.env，請記得修改。"
fi

if [ ! "$(docker -v)" ]; then
  sudo curl -sSL get.docker.com | sh
fi

if [ ! "$(docker-compose -v)" ]; then
  sudo curl -L "https://github.com/docker/compose/releases/download/$(curl -sL https://api.github.com/repos/docker/compose/releases/latest | grep tag_name | cut -d'"' -f 4)/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
fi

# composer install
sudo docker-compose pull
sudo docker-compose up -d
sudo docker-compose exec mongo mongorestore -h localhost --archive=/db.archive
sudo docker-compose exec chatbot bash -c "yarn; yarn seeder up"
sudo docker-compose exec admin-dashboard bash -c "cd /var/www/; composer install; yarn; yarn dev; php artisan key:generate"
sudo docker-compose exec message-gateway bash -c "cd /var/www/; composer install"

# 設定權限
chmod -R a=rwX admin-dashboard/{storage,bootstrap}
